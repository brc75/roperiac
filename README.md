# README #

This repo hosts our different environments for Terraform. 

	stage: an environment for non-production workloads (i.e. testing & development).
	prod: an environment for production workloads (i.e. user-facing apps).
	mgmt: an environment for DevOps tooling (e.g. bastion host, Jenkins).
	global: a place to put resources that are used across all environments, such as user management (IAM in AWS) and DNS management (Route53 in AWS).

Within each environment, we have separate folders for each “component.” The components differ for every project, but the typical ones are:

	vpc: the network topology for this environment.
	services: the apps or microservices to run in this environment, such as a CF frontend or a Wordpress installation. Each app lives in its own folder so it’s isolated from the others.
	data-storage: the data stores to run in this environment, such as PostgreSQL or Oracle. Each data store lives in its own folder so it’s isolated from the others.

Within each component, we have the actual Terraform templates, which we organize according to the following naming conventions:

	vars.tf: input variables.
	outputs.tf: output variables.
	main.tf: the actual resources.

**Issues**

If you’re using Terraform for a personal project, storing state in a local terraform.tfstate file works just fine. But if you want to use Terraform as a team on a real product, you run into several problems:

	Shared storage for state files: To be able to use Terraform to update our infrastructure, each of our team members needs access to the same Terraform state files. That means you need to store those files in a shared location.
	Locking state files: As soon as data is shared, you run into a new problem: locking. Without locking, if two team members are running Terraform at the same time, you may run into race conditions as multiple Terraform processes make concurrent updates to the state files, leading to conflicts, data loss, and state file corruption.
	Isolating state files: When making changes to our infrastructure, it’s a best practice to isolate different environments. For example, when making a change in the staging environment, you want to be sure that you’re not going to accidentally break production. But how can you isolate our changes if all of our infrastructure is defined in the same Terraform state file? We solve this by placing templates for each environment into its own folder. We go beyond the concept of just environments and down to the "component" level. This file layout makes it easy to browse the code and understand exactly what components are deployed in each environment. It also provides a good amount of isolation between environments and between components within an environment, ensuring that if something goes wrong, the damage is contained as much as possible to just one small part of our entire infrastructure.


Sources

https://blog.gruntwork.io/a-comprehensive-guide-to-terraform-b3d32832baca