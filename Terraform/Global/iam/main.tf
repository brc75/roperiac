##-----IAM Group-----##
resource "aws_iam_group" "Admins" {
    name = "Admins"
    path = "/"
}

resource "aws_iam_group" "CloudCheckr" {
    name = "CloudCheckr"
    path = "/"
}

resource "aws_iam_group" "ses-email-senders" {
    name = "ses-email-senders"
    path = "/"
}
##-----IAM Group Membership----##
resource "aws_iam_group_membership" "Admins" {
    name  = "Admins-group-membership"
    users = ["pea1"]
    group = "Admins"
}

resource "aws_iam_group_membership" "CloudCheckr" {
    name  = "CloudCheckr-group-membership"
    users = ["CloudCheckr"]
    group = "CloudCheckr"
}

resource "aws_iam_group_membership" "ses-email-senders" {
    name  = "ses-email-senders-group-membership"
    users = ["dd-ses-smtp-user"]
    group = "ses-email-senders"
}

##-----IAM Instance Profile-----##
resource "aws_iam_instance_profile" "aws-elasticbeanstalk-ec2-role" {
    name = "aws-elasticbeanstalk-ec2-role"
    path = "/"
    role = "aws-elasticbeanstalk-ec2-role"
}

resource "aws_iam_instance_profile" "ecsInstanceRole" {
    name = "ecsInstanceRole"
    path = "/"
    role = "ecsInstanceRole"
}

resource "aws_iam_instance_profile" "FlowLogsRole" {
    name = "FlowLogsRole"
    path = "/"
    role = "FlowLogsRole"
}

##-----IAM Policy----##
resource "aws_iam_policy" "CloudCheckr" {
    name        = "CloudCheckr"
    path        = "/"
    description = ""
    policy      = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "FullPolicy",
      "Action": [
        "acm:DescribeCertificate",
        "acm:ListCertificates",
        "acm:GetCertificate",
        "autoscaling:Describe*",
        "cloudformation:DescribeStacks",
        "cloudformation:GetStackPolicy",
        "cloudformation:GetTemplate",
        "cloudformation:ListStackResources",
        "cloudfront:List*",
        "cloudfront:GetDistributionConfig",
        "cloudfront:GetStreamingDistributionConfig",
        "cloudhsm:Describe*",
        "cloudhsm:List*",
        "cloudsearch:DescribeDomains",
        "cloudsearch:DescribeServiceAccessPolicies",
        "cloudsearch:DescribeStemmingOptions",
        "cloudsearch:DescribeStopwordOptions",
        "cloudsearch:DescribeSynonymOptions",
        "cloudsearch:DescribeDefaultSearchField",
        "cloudsearch:DescribeIndexFields",
        "cloudsearch:DescribeRankExpressions",
        "cloudtrail:DescribeTrails",
        "cloudtrail:GetTrailStatus",
        "cloudwatch:DescribeAlarms",
        "cloudwatch:GetMetricStatistics",
        "cloudwatch:ListMetrics",
        "config:DescribeDeliveryChannels",
        "config:DescribeDeliveryChannelStatus",
        "config:DescribeConfigurationRecorders",
        "config:DescribeConfigurationRecorderStatus",
        "datapipeline:ListPipelines",
        "datapipeline:GetPipelineDefinition",
        "datapipeline:DescribePipelines",
        "directconnect:DescribeLocations",
        "directconnect:DescribeConnections",
        "directconnect:DescribeVirtualInterfaces",
        "dynamodb:ListTables",
        "dynamodb:DescribeTable",
        "ec2:Describe*",
        "ec2:GetConsoleOutput",
        "ecs:ListClusters",
        "ecs:DescribeClusters",
        "ecs:ListContainerInstances",
        "ecs:DescribeContainerInstances",
        "ecs:ListServices",
        "ecs:DescribeServices",
        "ecs:ListTaskDefinitions",
        "ecs:DescribeTaskDefinition",
        "ecs:ListTasks",
        "ecs:DescribeTasks",
        "elasticache:DescribeCacheClusters",
        "elasticache:DescribeReservedCacheNodes",
        "elasticache:DescribeCacheSecurityGroups",
        "elasticache:DescribeCacheParameterGroups",
        "elasticache:DescribeCacheParameters",
        "elasticache:DescribeCacheSubnetGroups",
        "elasticbeanstalk:DescribeApplications",
        "elasticbeanstalk:DescribeConfigurationSettings",
        "elasticbeanstalk:DescribeEnvironments",
        "elasticbeanstalk:DescribeEvents",
        "elasticloadbalancing:DescribeLoadBalancers",
        "elasticloadbalancing:DescribeInstanceHealth",
        "elasticloadbalancing:DescribeLoadBalancerAttributes",
        "elasticloadbalancing:DescribeTags",
        "elasticmapreduce:DescribeJobFlows",
        "elasticmapreduce:DescribeStep",
        "elasticmapreduce:DescribeCluster",
        "elasticmapreduce:DescribeTags",
        "elasticmapreduce:ListSteps",
        "elasticmapreduce:ListInstanceGroups",
        "elasticmapreduce:ListBootstrapActions",
        "elasticmapreduce:ListClusters",
        "elasticmapreduce:ListInstances",
        "glacier:List*",
        "glacier:DescribeVault",
        "glacier:GetVaultNotifications",
        "glacier:DescribeJob",
        "glacier:GetJobOutput",
        "iam:Get*",
        "iam:List*",
        "iam:GenerateCredentialReport",
        "kinesis:ListStreams",
        "kinesis:DescribeStream",
        "kinesis:GetShardIterator",
        "kinesis:GetRecords",
        "lambda:ListFunctions",
        "rds:Describe*",
        "rds:ListTagsForResource",
        "redshift:Describe*",
        "redshift:ViewQueriesInConsole",
        "route53:ListHealthChecks",
        "route53:ListHostedZones",
        "route53:ListResourceRecordSets",
        "s3:GetBucketACL",
        "s3:GetBucketLocation",
        "s3:GetBucketLogging",
        "s3:GetBucketPolicy",
        "s3:GetBucketTagging",
        "s3:GetBucketWebsite",
        "s3:GetBucketNotification",
        "s3:GetLifecycleConfiguration",
        "s3:GetNotificationConfiguration",
        "s3:GetObject",
        "s3:GetObjectMetadata",
        "s3:List*",
        "sdb:ListDomains",
        "sdb:DomainMetadata",
        "ses:ListIdentities",
        "ses:GetSendStatistics",
        "ses:GetIdentityDkimAttributes",
        "ses:GetIdentityVerificationAttributes",
        "ses:GetSendQuota",
        "sns:GetSnsTopic",
        "sns:GetTopicAttributes",
        "sns:GetSubscriptionAttributes",
        "sns:ListTopics",
        "sns:ListSubscriptionsByTopic",
        "sqs:ListQueues",
        "sqs:GetQueueAttributes",
        "storagegateway:Describe*",
        "storagegateway:List*",
        "support:*",
        "swf:ListClosedWorkflowExecutions",
        "swf:ListDomains",
        "swf:ListActivityTypes",
        "swf:ListWorkflowTypes",
        "workspaces:DescribeWorkspaceDirectories",
        "workspaces:DescribeWorkspaceBundles",
        "workspaces:DescribeWorkspaces"
      ],
      "Effect": "Allow",
      "Resource": "*"
    },
    {
      "Sid": "CloudWatchLogsSpecific",
      "Effect": "Allow",
      "Action": [
        "logs:GetLogEvents",
        "logs:DescribeLogGroups",
        "logs:DescribeLogStreams"
      ],
      "Resource": [
        "arn:aws:logs:*:*:*"
      ]
    }
  ]
}
POLICY
}

resource "aws_iam_policy" "config-role_AWSConfigDeliveryPermissions_us-east-1" {
    name        = "config-role_AWSConfigDeliveryPermissions_us-east-1"
    path        = "/service-role/"
    description = ""
    policy      = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "s3:PutObject*"
      ],
      "Resource": [
        "arn:aws:s3:::cu-roper-config-bucket/AWSLogs/987257871045/*"
      ],
      "Condition": {
        "StringLike": {
          "s3:x-amz-acl": "bucket-owner-full-control"
        }
      }
    },
    {
      "Effect": "Allow",
      "Action": [
        "s3:GetBucketAcl"
      ],
      "Resource": "arn:aws:s3:::cu-roper-config-bucket"
    },
    {
      "Effect": "Allow",
      "Action": "sns:Publish",
      "Resource": "arn:aws:sns:us-east-1:987257871045:config-topic"
    }
  ]
}
POLICY
}

resource "aws_iam_policy" "Lucidchart_AWS_Import" {
    name        = "Lucidchart_AWS_Import"
    path        = "/"
    description = ""
    policy      = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "autoscaling:DescribeAutoScalingGroups",
        "autoscaling:DescribeLaunchConfigurations",
        "cloudfront:ListDistributions",
        "ec2:DescribeInstances",
        "ec2:DescribeNetworkAcls",
        "ec2:DescribeSecurityGroups",
        "ec2:DescribeSubnets",
        "ec2:DescribeVolumes",
        "ec2:DescribeVpcs",
        "elasticloadbalancing:DescribeLoadBalancers",
        "iam:GetGroupPolicy",
        "iam:GetPolicy",
        "iam:GetPolicyVersion",
        "iam:GetRolePolicy",
        "iam:GetUserPolicy",
        "iam:ListAttachedGroupPolicies",
        "iam:ListAttachedRolePolicies",
        "iam:ListAttachedUserPolicies",
        "iam:ListGroupPolicies",
        "iam:ListGroups",
        "iam:ListGroupsForUser",
        "iam:ListRolePolicies",
        "iam:ListRoles",
        "iam:ListUserPolicies",
        "iam:ListUsers",
        "rds:DescribeDBInstances",
        "s3:ListAllMyBuckets",
        "SNS:GetTopicAttributes",
        "SNS:ListTopics",
        "sqs:GetQueueAttributes",
        "sqs:ListQueues"
      ],
      "Resource": [
        "*"
      ]
    }
  ]
}
POLICY
}

resource "aws_iam_policy" "ses-send-email" {
    name        = "ses-send-email"
    path        = "/"
    description = "Allows sending email via SES."
    policy      = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "ses:SendBounce",
        "ses:SendEmail",
        "ses:SendRawEmail"
      ],
      "Resource": "*",
      "Effect": "Allow"
    }
  ]
}
POLICY
}

##-----IAM Policy Attachment-----##
resource "aws_iam_policy_attachment" "CloudCheckr-policy-attachment" {
    name       = "CloudCheckr-policy-attachment"
    policy_arn = "arn:aws:iam::987257871045:policy/CloudCheckr"
    groups     = ["CloudCheckr"]
    users      = []
    roles      = []
}

resource "aws_iam_policy_attachment" "config-role_AWSConfigDeliveryPermissions_us-east-1-policy-attachment" {
    name       = "config-role_AWSConfigDeliveryPermissions_us-east-1-policy-attachment"
    policy_arn = "arn:aws:iam::987257871045:policy/service-role/config-role_AWSConfigDeliveryPermissions_us-east-1"
    groups     = []
    users      = []
    roles      = ["config-role"]
}

resource "aws_iam_policy_attachment" "ses-send-email-policy-attachment" {
    name       = "ses-send-email-policy-attachment"
    policy_arn = "arn:aws:iam::987257871045:policy/ses-send-email"
    groups     = ["ses-email-senders"]
    users      = []
    roles      = []
}

resource "aws_iam_policy_attachment" "AmazonRDSFullAccess-policy-attachment" {
    name       = "AmazonRDSFullAccess-policy-attachment"
    policy_arn = "arn:aws:iam::aws:policy/AmazonRDSFullAccess"
    groups     = []
    users      = []
    roles      = ["shib-RDSAdmin"]
}

resource "aws_iam_policy_attachment" "AmazonEC2FullAccess-policy-attachment" {
    name       = "AmazonEC2FullAccess-policy-attachment"
    policy_arn = "arn:aws:iam::aws:policy/AmazonEC2FullAccess"
    groups     = []
    users      = ["bcruzS3cli", "jenkins"]
    roles      = []
}

resource "aws_iam_policy_attachment" "AWSLambdaFullAccess-policy-attachment" {
    name       = "AWSLambdaFullAccess-policy-attachment"
    policy_arn = "arn:aws:iam::aws:policy/AWSLambdaFullAccess"
    groups     = []
    users      = ["bcruzS3cli"]
    roles      = []
}

resource "aws_iam_policy_attachment" "AmazonS3FullAccess-policy-attachment" {
    name       = "AmazonS3FullAccess-policy-attachment"
    policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
    groups     = []
    users      = ["bcruzS3cli", "jenkinsbackup", "s3fs", "s3rss", "s3wordpress"]
    roles      = []
}

resource "aws_iam_policy_attachment" "AWSElasticBeanstalkEnhancedHealth-policy-attachment" {
    name       = "AWSElasticBeanstalkEnhancedHealth-policy-attachment"
    policy_arn = "arn:aws:iam::aws:policy/service-role/AWSElasticBeanstalkEnhancedHealth"
    groups     = []
    users      = []
    roles      = ["aws-elasticbeanstalk-service-role"]
}

resource "aws_iam_policy_attachment" "ReadOnlyAccess-policy-attachment" {
    name       = "ReadOnlyAccess-policy-attachment"
    policy_arn = "arn:aws:iam::aws:policy/ReadOnlyAccess"
    groups     = []
    users      = []
    roles      = ["shib-cs"]
}

resource "aws_iam_policy_attachment" "AWSElasticLoadBalancingServiceRolePolicy-policy-attachment" {
    name       = "AWSElasticLoadBalancingServiceRolePolicy-policy-attachment"
    policy_arn = "arn:aws:iam::aws:policy/aws-service-role/AWSElasticLoadBalancingServiceRolePolicy"
    groups     = []
    users      = []
    roles      = ["AWSServiceRoleForElasticLoadBalancing"]
}

resource "aws_iam_policy_attachment" "AmazonRDSServiceRolePolicy-policy-attachment" {
    name       = "AmazonRDSServiceRolePolicy-policy-attachment"
    policy_arn = "arn:aws:iam::aws:policy/aws-service-role/AmazonRDSServiceRolePolicy"
    groups     = []
    users      = []
    roles      = ["AWSServiceRoleForRDS"]
}

resource "aws_iam_policy_attachment" "AWSOrganizationsServiceTrustPolicy-policy-attachment" {
    name       = "AWSOrganizationsServiceTrustPolicy-policy-attachment"
    policy_arn = "arn:aws:iam::aws:policy/aws-service-role/AWSOrganizationsServiceTrustPolicy"
    groups     = []
    users      = []
    roles      = ["AWSServiceRoleForOrganizations"]
}

resource "aws_iam_policy_attachment" "AWSConfigRole-policy-attachment" {
    name       = "AWSConfigRole-policy-attachment"
    policy_arn = "arn:aws:iam::aws:policy/service-role/AWSConfigRole"
    groups     = []
    users      = []
    roles      = ["config-role"]
}

resource "aws_iam_policy_attachment" "AWSElasticBeanstalkWebTier-policy-attachment" {
    name       = "AWSElasticBeanstalkWebTier-policy-attachment"
    policy_arn = "arn:aws:iam::aws:policy/AWSElasticBeanstalkWebTier"
    groups     = []
    users      = []
    roles      = ["aws-elasticbeanstalk-ec2-role"]
}

resource "aws_iam_policy_attachment" "AmazonKinesisFullAccess-policy-attachment" {
    name       = "AmazonKinesisFullAccess-policy-attachment"
    policy_arn = "arn:aws:iam::aws:policy/AmazonKinesisFullAccess"
    groups     = []
    users      = ["bcruzS3cli"]
    roles      = []
}

resource "aws_iam_policy_attachment" "AmazonECSServiceRolePolicy-policy-attachment" {
    name       = "AmazonECSServiceRolePolicy-policy-attachment"
    policy_arn = "arn:aws:iam::aws:policy/aws-service-role/AmazonECSServiceRolePolicy"
    groups     = []
    users      = []
    roles      = ["AWSServiceRoleForECS"]
}

resource "aws_iam_policy_attachment" "AdministratorAccess-policy-attachment" {
    name       = "AdministratorAccess-policy-attachment"
    policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
    groups     = ["Admins"]
    users      = ["terransible"]
    roles      = ["shib-admin"]
}

resource "aws_iam_policy_attachment" "AWSElasticBeanstalkMulticontainerDocker-policy-attachment" {
    name       = "AWSElasticBeanstalkMulticontainerDocker-policy-attachment"
    policy_arn = "arn:aws:iam::aws:policy/AWSElasticBeanstalkMulticontainerDocker"
    groups     = []
    users      = []
    roles      = ["aws-elasticbeanstalk-ec2-role"]
}

resource "aws_iam_policy_attachment" "AmazonEC2ContainerServiceFullAccess-policy-attachment" {
    name       = "AmazonEC2ContainerServiceFullAccess-policy-attachment"
    policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerServiceFullAccess"
    groups     = []
    users      = ["bcruzS3cli"]
    roles      = []
}

resource "aws_iam_policy_attachment" "AmazonEC2ContainerRegistryPowerUser-policy-attachment" {
    name       = "AmazonEC2ContainerRegistryPowerUser-policy-attachment"
    policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryPowerUser"
    groups     = []
    users      = ["jenkins"]
    roles      = []
}

resource "aws_iam_policy_attachment" "AWSElasticBeanstalkService-policy-attachment" {
    name       = "AWSElasticBeanstalkService-policy-attachment"
    policy_arn = "arn:aws:iam::aws:policy/service-role/AWSElasticBeanstalkService"
    groups     = []
    users      = []
    roles      = ["aws-elasticbeanstalk-service-role"]
}

resource "aws_iam_policy_attachment" "AmazonElasticFileSystemFullAccess-policy-attachment" {
    name       = "AmazonElasticFileSystemFullAccess-policy-attachment"
    policy_arn = "arn:aws:iam::aws:policy/AmazonElasticFileSystemFullAccess"
    groups     = []
    users      = ["bcruzS3cli"]
    roles      = []
}

resource "aws_iam_policy_attachment" "AmazonEC2ContainerServiceforEC2Role-policy-attachment" {
    name       = "AmazonEC2ContainerServiceforEC2Role-policy-attachment"
    policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
    groups     = []
    users      = ["jenkins"]
    roles      = ["ecsInstanceRole"]
}

resource "aws_iam_policy_attachment" "AmazonEC2ContainerServiceRole-policy-attachment" {
    name       = "AmazonEC2ContainerServiceRole-policy-attachment"
    policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceRole"
    groups     = []
    users      = []
    roles      = ["ecsServiceRole"]
}

resource "aws_iam_policy_attachment" "AWSElasticBeanstalkWorkerTier-policy-attachment" {
    name       = "AWSElasticBeanstalkWorkerTier-policy-attachment"
    policy_arn = "arn:aws:iam::aws:policy/AWSElasticBeanstalkWorkerTier"
    groups     = []
    users      = []
    roles      = ["aws-elasticbeanstalk-ec2-role"]
}

resource "aws_iam_policy_attachment" "AmazonRDSEnhancedMonitoringRole-policy-attachment" {
    name       = "AmazonRDSEnhancedMonitoringRole-policy-attachment"
    policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonRDSEnhancedMonitoringRole"
    groups     = []
    users      = []
    roles      = ["rds-monitoring-role"]
}

##-----IAM Roles-----##
resource "aws_iam_role" "aws-elasticbeanstalk-ec2-role" {
    name               = "aws-elasticbeanstalk-ec2-role"
    path               = "/"
    assume_role_policy = <<POLICY
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role" "aws-elasticbeanstalk-service-role" {
    name               = "aws-elasticbeanstalk-service-role"
    path               = "/"
    assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "elasticbeanstalk.amazonaws.com"
      },
      "Action": "sts:AssumeRole",
      "Condition": {
        "StringEquals": {
          "sts:ExternalId": "elasticbeanstalk"
        }
      }
    }
  ]
}
POLICY
}

resource "aws_iam_role" "AWSServiceRoleForECS" {
    name               = "AWSServiceRoleForECS"
    path               = "/aws-service-role/ecs.amazonaws.com/"
    assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ecs.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role" "AWSServiceRoleForElasticLoadBalancing" {
    name               = "AWSServiceRoleForElasticLoadBalancing"
    path               = "/aws-service-role/elasticloadbalancing.amazonaws.com/"
    assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "elasticloadbalancing.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role" "AWSServiceRoleForOrganizations" {
    name               = "AWSServiceRoleForOrganizations"
    path               = "/aws-service-role/organizations.amazonaws.com/"
    assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "organizations.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role" "AWSServiceRoleForRDS" {
    name               = "AWSServiceRoleForRDS"
    path               = "/aws-service-role/rds.amazonaws.com/"
    assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "rds.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role" "config-role" {
    name               = "config-role"
    path               = "/"
    assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "config.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    },
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "AWS": [
          "arn:aws:iam::799199334739:root",
          "arn:aws:iam::627590677832:root"
        ]
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role" "ecsInstanceRole" {
    name               = "ecsInstanceRole"
    path               = "/"
    assume_role_policy = <<POLICY
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role" "ecsServiceRole" {
    name               = "ecsServiceRole"
    path               = "/"
    assume_role_policy = <<POLICY
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "ecs.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role" "FlowLogsRole" {
    name               = "FlowLogsRole"
    path               = "/"
    assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "vpc-flow-logs.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role" "rds-monitoring-role" {
    name               = "rds-monitoring-role"
    path               = "/"
    assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "monitoring.rds.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role" "shib-admin" {
    name               = "shib-admin"
    path               = "/"
    assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Federated": "arn:aws:iam::987257871045:saml-provider/cornell_idp"
      },
      "Action": "sts:AssumeRoleWithSAML",
      "Condition": {
        "StringEquals": {
          "SAML:aud": "https://signin.aws.amazon.com/saml"
        }
      }
    }
  ]
}
POLICY
}

resource "aws_iam_role" "shib-cs" {
    name               = "shib-cs"
    path               = "/"
    assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Federated": "arn:aws:iam::987257871045:saml-provider/cornell_idp"
      },
      "Action": "sts:AssumeRoleWithSAML",
      "Condition": {
        "StringEquals": {
          "SAML:aud": "https://signin.aws.amazon.com/saml"
        }
      }
    }
  ]
}
POLICY
}

resource "aws_iam_role" "shib-RDSAdmin" {
    name               = "shib-RDSAdmin"
    path               = "/"
    assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Federated": "arn:aws:iam::987257871045:saml-provider/cornell_idp"
      },
      "Action": "sts:AssumeRoleWithSAML",
      "Condition": {
        "StringEquals": {
          "SAML:aud": "https://signin.aws.amazon.com/saml"
        }
      }
    }
  ]
}
POLICY
}

##------IAM Role Policy-----##

resource "aws_iam_role_policy" "config-role_oneClick_config-role_1456929451390" {
    name   = "oneClick_config-role_1456929451390"
    role   = "config-role"
    policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "appstream:Get*",
        "autoscaling:Describe*",
        "cloudformation:DescribeStacks",
        "cloudformation:DescribeStackEvents",
        "cloudformation:DescribeStackResource",
        "cloudformation:DescribeStackResources",
        "cloudformation:GetTemplate",
        "cloudformation:List*",
        "cloudfront:Get*",
        "cloudfront:List*",
        "cloudtrail:DescribeTrails",
        "cloudtrail:GetTrailStatus",
        "cloudwatch:Describe*",
        "cloudwatch:Get*",
        "cloudwatch:List*",
        "config:Put*",
        "directconnect:Describe*",
        "dynamodb:GetItem",
        "dynamodb:BatchGetItem",
        "dynamodb:Query",
        "dynamodb:Scan",
        "dynamodb:DescribeTable",
        "dynamodb:ListTables",
        "ec2:Describe*",
        "elasticache:Describe*",
        "elasticbeanstalk:Check*",
        "elasticbeanstalk:Describe*",
        "elasticbeanstalk:List*",
        "elasticbeanstalk:RequestEnvironmentInfo",
        "elasticbeanstalk:RetrieveEnvironmentInfo",
        "elasticloadbalancing:Describe*",
        "elastictranscoder:Read*",
        "elastictranscoder:List*",
        "iam:List*",
        "iam:Get*",
        "kinesis:Describe*",
        "kinesis:Get*",
        "kinesis:List*",
        "opsworks:Describe*",
        "opsworks:Get*",
        "route53:Get*",
        "route53:List*",
        "redshift:Describe*",
        "redshift:ViewQueriesInConsole",
        "rds:Describe*",
        "rds:ListTagsForResource",
        "s3:Get*",
        "s3:List*",
        "sdb:GetAttributes",
        "sdb:List*",
        "sdb:Select*",
        "ses:Get*",
        "ses:List*",
        "sns:Get*",
        "sns:List*",
        "sqs:GetQueueAttributes",
        "sqs:ListQueues",
        "sqs:ReceiveMessage",
        "storagegateway:List*",
        "storagegateway:Describe*",
        "trustedadvisor:Describe*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "s3:PutObject*"
      ],
      "Resource": [
        "arn:aws:s3:::cu-roper-config-bucket/AWSLogs/987257871045/*"
      ],
      "Condition": {
        "StringLike": {
          "s3:x-amz-acl": "bucket-owner-full-control"
        }
      }
    },
    {
      "Effect": "Allow",
      "Action": [
        "s3:GetBucketAcl"
      ],
      "Resource": "arn:aws:s3:::cu-roper-config-bucket"
    },
    {
      "Effect": "Allow",
      "Action": "sns:Publish",
      "Resource": "arn:aws:sns:us-east-1:987257871045:config-topic"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy" "FlowLogsRole_CustomPolicy" {
    name   = "CustomPolicy"
    role   = "FlowLogsRole"
    policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "logs:DescribeLogGroups",
        "logs:DescribeLogStreams"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
POLICY
}

##-----IAM User-----##
resource "aws_iam_user" "bcruzS3cli" {
    name = "bcruzS3cli"
    path = "/"
}

resource "aws_iam_user" "CloudCheckr" {
    name = "CloudCheckr"
    path = "/"
}

resource "aws_iam_user" "dd-ses-smtp-user" {
    name = "dd-ses-smtp-user"
    path = "/holding-id/ses/"
}

resource "aws_iam_user" "jenkins" {
    name = "jenkins"
    path = "/"
}

resource "aws_iam_user" "jenkinsbackup" {
    name = "jenkinsbackup"
    path = "/"
}

resource "aws_iam_user" "pea1" {
    name = "pea1"
    path = "/"
}

resource "aws_iam_user" "s3fs" {
    name = "s3fs"
    path = "/"
}

resource "aws_iam_user" "s3rss" {
    name = "s3rss"
    path = "/"
}

resource "aws_iam_user" "s3wordpress" {
    name = "s3wordpress"
    path = "/"
}

resource "aws_iam_user" "ses-smtp-user-20170927-144544" {
    name = "ses-smtp-user.20170927-144544"
    path = "/"
}

resource "aws_iam_user" "terransible" {
    name = "terransible"
    path = "/"
}

##-----IAM User Policy-----##
resource "aws_iam_user_policy" "ses-smtp-user-20170927-144544_AmazonSesSendingAccess" {
    name   = "AmazonSesSendingAccess"
    user   = "ses-smtp-user.20170927-144544"
    policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": "ses:SendRawEmail",
      "Resource": "*"
    }
  ]
}
POLICY
}
