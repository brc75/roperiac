##-----Route53 Records-----##
resource "aws_route53_record" "roper-cucloud-net-A" {
    zone_id = "Z1NAQZNNKT5D8N"
    name    = "roper.cucloud.net"
    type    = "A"

    alias {
        name    = "dualstack.gatekeeper-lb-209335347.us-east-1.elb.amazonaws.com"
        zone_id = "Z35SXDOTRQ7X7K"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "roper-cucloud-net-NS" {
    zone_id = "Z1NAQZNNKT5D8N"
    name    = "roper.cucloud.net"
    type    = "NS"
    records = ["ns-1320.awsdns-37.org.", "ns-790.awsdns-34.net.", "ns-1863.awsdns-40.co.uk.", "ns-338.awsdns-42.com."]
    ttl     = "172800"

}

resource "aws_route53_record" "roper-cucloud-net-SOA" {
    zone_id = "Z1NAQZNNKT5D8N"
    name    = "roper.cucloud.net"
    type    = "SOA"
    records = ["ns-1320.awsdns-37.org. awsdns-hostmaster.amazon.com. 1 7200 900 1209600 86400"]
    ttl     = "900"

}

resource "aws_route53_record" "roper-center-A" {
    zone_id = "Z2OSTCBXMIM7NA"
    name    = "roper.center"
    type    = "A"

    alias {
        name    = "dualstack.gatekeeper-lb-209335347.us-east-1.elb.amazonaws.com"
        zone_id = "Z35SXDOTRQ7X7K"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "roper-center-NS" {
    zone_id = "Z2OSTCBXMIM7NA"
    name    = "roper.center"
    type    = "NS"
    records = ["ns-1324.awsdns-37.org.", "ns-1840.awsdns-38.co.uk.", "ns-544.awsdns-04.net.", "ns-49.awsdns-06.com."]
    ttl     = "172800"

}

resource "aws_route53_record" "roper-center-SOA" {
    zone_id = "Z2OSTCBXMIM7NA"
    name    = "roper.center"
    type    = "SOA"
    records = ["ns-1324.awsdns-37.org. awsdns-hostmaster.amazon.com. 1 7200 900 1209600 86400"]
    ttl     = "900"

}

resource "aws_route53_record" "_amazonses-roper-center-TXT" {
    zone_id = "Z2OSTCBXMIM7NA"
    name    = "_amazonses.roper.center"
    type    = "TXT"
    records = ["\"abDiQ5d0Wco79OhsJjh/OaR7qVLgmp5pqTmtOm9rjmw=\""]
    ttl     = "1800"

}

resource "aws_route53_record" "enlmkayfvkmofb6cxdjtziw6gxjie4zb-_domainkey-roper-center-CNAME" {
    zone_id = "Z2OSTCBXMIM7NA"
    name    = "enlmkayfvkmofb6cxdjtziw6gxjie4zb._domainkey.roper.center"
    type    = "CNAME"
    records = ["enlmkayfvkmofb6cxdjtziw6gxjie4zb.dkim.amazonses.com"]
    ttl     = "1800"

}

resource "aws_route53_record" "gw2ixmnp4k6jhhlo4m54owqax6p2dcg6-_domainkey-roper-center-CNAME" {
    zone_id = "Z2OSTCBXMIM7NA"
    name    = "gw2ixmnp4k6jhhlo4m54owqax6p2dcg6._domainkey.roper.center"
    type    = "CNAME"
    records = ["gw2ixmnp4k6jhhlo4m54owqax6p2dcg6.dkim.amazonses.com"]
    ttl     = "1800"

}

resource "aws_route53_record" "kqeybqyxnjzm6uji2n35faqtqzn6msxd-_domainkey-roper-center-CNAME" {
    zone_id = "Z2OSTCBXMIM7NA"
    name    = "kqeybqyxnjzm6uji2n35faqtqzn6msxd._domainkey.roper.center"
    type    = "CNAME"
    records = ["kqeybqyxnjzm6uji2n35faqtqzn6msxd.dkim.amazonses.com"]
    ttl     = "1800"

}

resource "aws_route53_record" "dashboard-roper-center-CNAME" {
    zone_id = "Z2OSTCBXMIM7NA"
    name    = "dashboard.roper.center"
    type    = "CNAME"
    records = ["stats.uptimerobot.com"]
    ttl     = "300"

}

resource "aws_route53_record" "demo-roper-center-A" {
    zone_id = "Z2OSTCBXMIM7NA"
    name    = "demo.roper.center"
    type    = "A"

    alias {
        name    = "dualstack.presidentiallb-2114248935.us-east-1.elb.amazonaws.com"
        zone_id = "Z35SXDOTRQ7X7K"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "deposit-roper-center-A" {
    zone_id = "Z2OSTCBXMIM7NA"
    name    = "deposit.roper.center"
    type    = "A"

    alias {
        name    = "dualstack.presidentiallb-2114248935.us-east-1.elb.amazonaws.com"
        zone_id = "Z35SXDOTRQ7X7K"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "files-roper-center-A" {
    zone_id = "Z2OSTCBXMIM7NA"
    name    = "files.roper.center"
    type    = "A"

    alias {
        name    = "s3-website-us-east-1.amazonaws.com"
        zone_id = "Z3AQBSTGFYJSTF"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "internal-roper-center-A" {
    zone_id = "Z2OSTCBXMIM7NA"
    name    = "internal.roper.center"
    type    = "A"

    alias {
        name    = "dualstack.cuonlylb-933906960.us-east-1.elb.amazonaws.com"
        zone_id = "Z35SXDOTRQ7X7K"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "internaltest-roper-center-A" {
    zone_id = "Z2OSTCBXMIM7NA"
    name    = "internaltest.roper.center"
    type    = "A"

    alias {
        name    = "dualstack.internal-testinternal-1266693361.us-east-1.elb.amazonaws.com"
        zone_id = "Z35SXDOTRQ7X7K"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "logs-roper-center-CNAME" {
    zone_id = "Z2OSTCBXMIM7NA"
    name    = "logs.roper.center"
    type    = "CNAME"
    records = ["graylog.roper.cornell.edu"]
    ttl     = "300"

}

resource "aws_route53_record" "old-roper-center-CNAME" {
    zone_id = "Z2OSTCBXMIM7NA"
    name    = "old.roper.center"
    type    = "CNAME"
    records = ["sf-roper01.serverfarm.cornell.edu"]
    ttl     = "300"

}

resource "aws_route53_record" "pewglobal-roper-center-A" {
    zone_id = "Z2OSTCBXMIM7NA"
    name    = "pewglobal.roper.center"
    type    = "A"

    alias {
        name    = "dualstack.gatekeeper-lb-209335347.us-east-1.elb.amazonaws.com"
        zone_id = "Z35SXDOTRQ7X7K"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "presidential-roper-center-A" {
    zone_id = "Z2OSTCBXMIM7NA"
    name    = "presidential.roper.center"
    type    = "A"

    alias {
        name    = "dualstack.presidentiallb-2114248935.us-east-1.elb.amazonaws.com"
        zone_id = "Z35SXDOTRQ7X7K"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "preview-roper-center-A" {
    zone_id = "Z2OSTCBXMIM7NA"
    name    = "preview.roper.center"
    type    = "A"

    alias {
        name    = "dualstack.presidentiallb-2114248935.us-east-1.elb.amazonaws.com"
        zone_id = "Z35SXDOTRQ7X7K"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "sftp-roper-center-CNAME" {
    zone_id = "Z2OSTCBXMIM7NA"
    name    = "sftp.roper.center"
    type    = "CNAME"
    records = ["sf-roper02.serverfarm.cornell.edu"]
    ttl     = "300"

}

resource "aws_route53_record" "status-roper-center-A" {
    zone_id = "Z2OSTCBXMIM7NA"
    name    = "status.roper.center"
    type    = "A"

    alias {
        name    = "dualstack.presidentiallb-2114248935.us-east-1.elb.amazonaws.com"
        zone_id = "Z35SXDOTRQ7X7K"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "upload-beta-roper-center-A" {
    zone_id = "Z2OSTCBXMIM7NA"
    name    = "upload-beta.roper.center"
    type    = "A"

    alias {
        name    = "dualstack.presidentiallb-2114248935.us-east-1.elb.amazonaws.com"
        zone_id = "Z35SXDOTRQ7X7K"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "upload-roper-center-A" {
    zone_id = "Z2OSTCBXMIM7NA"
    name    = "upload.roper.center"
    type    = "A"

    alias {
        name    = "dualstack.presidentiallb-2114248935.us-east-1.elb.amazonaws.com"
        zone_id = "Z35SXDOTRQ7X7K"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "wiki-roper-center-CNAME" {
    zone_id = "Z2OSTCBXMIM7NA"
    name    = "wiki.roper.center"
    type    = "CNAME"
    records = ["roperdocker.roper.cornell.edu"]
    ttl     = "300"

}

##-----Route53 Hosted Zones-----##

resource "aws_route53_zone" "roper-cucloud-net-public" {
    name       = "roper.cucloud.net"
    comment    = ""

    tags {
    }
}

resource "aws_route53_zone" "roper-center-public" {
    name       = "roper.center"
    comment    = "Super fancy domain."

    tags {
    }
}