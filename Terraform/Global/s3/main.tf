##-----S3-----##
resource "aws_s3_bucket" "archive-roper-center" {
    bucket = "archive.roper.center"
    acl    = "private"
    policy = <<POLICY
{
  "Version": "2012-10-17",
  "Id": "S3-Console-Auto-Gen-Policy-1512585686772",
  "Statement": [
    {
      "Sid": "S3PolicyStmt-DO-NOT-MODIFY-1512585686772",
      "Effect": "Allow",
      "Principal": {
        "Service": "s3.amazonaws.com"
      },
      "Action": "s3:PutObject",
      "Resource": "arn:aws:s3:::archive.roper.center/*",
      "Condition": {
        "StringEquals": {
          "aws:SourceAccount": "987257871045",
          "s3:x-amz-acl": "bucket-owner-full-control"
        },
        "ArnLike": {
          "aws:SourceArn": "arn:aws:s3:::archive.roper.center"
        }
      }
    }
  ]
}
POLICY
}

resource "aws_s3_bucket" "cf-templates-1v1wn8ayordt7-us-east-1" {
    bucket = "cf-templates-1v1wn8ayordt7-us-east-1"
    acl    = "private"
}

resource "aws_s3_bucket" "cu-roper-cloud-trail" {
    bucket = "cu-roper-cloud-trail"
    acl    = "private"
    policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "AWSCloudTrailAclCheck20131101",
      "Effect": "Allow",
      "Principal": {
        "Service": "cloudtrail.amazonaws.com"
      },
      "Action": "s3:GetBucketAcl",
      "Resource": "arn:aws:s3:::cu-roper-cloud-trail"
    },
    {
      "Sid": "AWSCloudTrailWrite20131101",
      "Effect": "Allow",
      "Principal": {
        "Service": "cloudtrail.amazonaws.com"
      },
      "Action": "s3:PutObject",
      "Resource": "arn:aws:s3:::cu-roper-cloud-trail/AWSLogs/987257871045/*",
      "Condition": {
        "StringEquals": {
          "s3:x-amz-acl": "bucket-owner-full-control"
        }
      }
    }
  ]
}
POLICY
}

resource "aws_s3_bucket" "cu-roper-config-bucket" {
    bucket = "cu-roper-config-bucket"
    acl    = "private"
}

resource "aws_s3_bucket" "cu-roper-logs" {
    bucket = "cu-roper-logs"
    acl    = "private"
    policy = <<POLICY
{
  "Version": "2012-10-17",
  "Id": "Policy1478634972043",
  "Statement": [
    {
      "Sid": "Stmt1478634968422",
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::127311923021:root"
      },
      "Action": "s3:PutObject",
      "Resource": "arn:aws:s3:::cu-roper-logs/AWSLogs/987257871045/*"
    }
  ]
}
POLICY
}

resource "aws_s3_bucket" "elasticbeanstalk-us-east-1-987257871045" {
    bucket = "elasticbeanstalk-us-east-1-987257871045"
    acl    = "private"
    policy = <<POLICY
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Sid": "eb-ad78f54a-f239-4c90-adda-49e5f56cb51e",
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::987257871045:role/aws-elasticbeanstalk-ec2-role"
      },
      "Action": "s3:PutObject",
      "Resource": "arn:aws:s3:::elasticbeanstalk-us-east-1-987257871045/resources/environments/logs/*"
    },
    {
      "Sid": "eb-af163bf3-d27b-4712-b795-d1e33e331ca4",
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::987257871045:role/aws-elasticbeanstalk-ec2-role"
      },
      "Action": [
        "s3:ListBucket",
        "s3:ListBucketVersions",
        "s3:GetObject",
        "s3:GetObjectVersion"
      ],
      "Resource": [
        "arn:aws:s3:::elasticbeanstalk-us-east-1-987257871045",
        "arn:aws:s3:::elasticbeanstalk-us-east-1-987257871045/resources/environments/*"
      ]
    },
    {
      "Sid": "eb-58950a8c-feb6-11e2-89e0-0800277d041b",
      "Effect": "Deny",
      "Principal": {
        "AWS": "*"
      },
      "Action": "s3:DeleteBucket",
      "Resource": "arn:aws:s3:::elasticbeanstalk-us-east-1-987257871045"
    }
  ]
}
POLICY
}

resource "aws_s3_bucket" "files-roper-center" {
    bucket = "files.roper.center"
    acl    = "private"
    policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "Allow Public Access to All Objects",
      "Effect": "Allow",
      "Principal": "*",
      "Action": "s3:GetObject",
      "Resource": "arn:aws:s3:::files.roper.center/*"
    }
  ]
}
POLICY
}

resource "aws_s3_bucket" "it-roper-center" {
    bucket = "it.roper.center"
    acl    = "private"
}

resource "aws_s3_bucket" "newarchive-roper-center" {
    bucket = "newarchive.roper.center"
    acl    = "private"
}

resource "aws_s3_bucket" "ous-roper-center" {
    bucket = "ous.roper.center"
    acl    = "private"
}